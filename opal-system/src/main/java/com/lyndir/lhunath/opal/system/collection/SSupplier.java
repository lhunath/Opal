package com.lyndir.lhunath.opal.system.collection;

import com.google.common.base.Supplier;
import java.io.Serializable;


/**
 * <i>09 10, 2011</i>
 *
 * @author lhunath
 */
public interface SSupplier<T> extends Supplier<T>, Serializable {

}
